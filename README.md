# update-ip-in-ssh-configs

A quick and dirty Bash script to update the IP address of your host in SSH config files on other machines

## Features

* Simple, does what it should do. No more, no less
* Dependencies that are normal on a regular Linux system

## Dependencies

* [curl](https://curl.se/) - For fetching the background images

## Installation
1. Make sure the script is executable by running `chmod +x update-ip-in-ssh-configs` on it.
2. Put `update-ip-in-ssh-configs` in your `/bin/`, `/usr/bin/`, `~/bin/`, or any other directory in your PATH environment variable.

## Usage

The usage help for the command is:
```
Usage: update-ip-in-ssh-configs [ARGUMENTS]

Arguments:
  -h, --help                  Displays this help text
  -V, --version               Displays the version number
  -v, --verbose               Displays more information
  -H, --host HOST             Specifies which host to update (default is your current hostname)
  -i, --ip IP                 Specifies an IP address to use as the new IP
                              Default is the one provided from the IP provider
  -I, --ip-provider HOST      Specifies which provider to get your new IP from using cURL
                              Default is "ifconfig.me"
  -r, --remote-hostname NAME  Specifies the remote hostname on which to update the ssh config
                              -r can be specified multiple times, to update several targets
  -a, --all                   Uses all hosts in your ssh config as remote targets
  -b, --backup                Backs up the config file on the remote machine
  -f, --date-format FORMAT    Sets the format for the appended date on the backup config
                              The format is the format the date command uses
                              Default is "_%Y%m%d_%H%M%S"
  -d, --dry                   Performs a dry run; does everything except replacing the config
```

If you have followed the installation instructions you can just run:
```
$ update-ip-in-ssh-configs
```

If you don't have it installed, you can of course still run it as-is with one of these commands:
```
$ ./update-ip-in-ssh-configs
$ bash update-ip-in-ssh-configs
```

## Copyright

This application is released under the `WTFPL`
```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 ```
